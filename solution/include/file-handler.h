//
// Created by kasym on 11/14/2023.
//

#ifndef FILE_HANDLER_H
#define FILE_HANDLER_H
#include <stdio.h>
enum file_operation_status {
    FILE_OPER_OK = 0,
    FILE_OPER_ERROR
};

struct file_operation_result* open_specified_file_for_reading(char* name);
struct file_operation_result* open_specified_file_for_writing(char* name);
void close_file(FILE* file);


struct file_operation_result {
    FILE* file;
    enum file_operation_status status;
};

#endif //FILE_HANDLER_H
