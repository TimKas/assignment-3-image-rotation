//
// Created by kasym on 11/14/2023.
//

#ifndef IMAGE_HANDLER_H
#define IMAGE_HANDLER_H
#include <stdint.h>
#include <stdio.h>

enum image_operation_status {
    IMG_OPER_OK = 0,
    READ_ERROR,
    WRITE_ERROR,
    PADDING_ISSUES
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};


struct image {
    uint64_t width, height;
    struct pixel *data;
};
uint32_t calc_padding(uint32_t const width);
struct image* create_new_image(uint32_t h, uint32_t w);
enum image_operation_status read_image_from_file(FILE* file, struct image const* to);
enum image_operation_status save_image_to_file(FILE* file, struct image const* from);
uint32_t getSizeImage(const uint32_t width, const uint32_t height);

void release_image(struct image* image);


#endif //IMAGE_HANDLER_H
