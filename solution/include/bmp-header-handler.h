
//
// Created by kasym on 11/14/2023.
//

#ifndef BMP_HEADER_HANDLER_H
#define BMP_HEADER_HANDLER_H
#include <stdint.h>
#include <stdio.h>

enum bmp_file_header_status {
    BMP_HEADER_OK = 0,
    BMP_HEADER_READ_ERROR,
    BMP_HEADER_WRITE_ERROR,
    PADDING_ERROR
};

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
struct bmp_header_operation_result {
    enum bmp_file_header_status status;
    struct bmp_header* header;
};

struct bmp_header_operation_result* read_bmp_header(FILE* file);
enum bmp_file_header_status write_bmp_header(FILE* file, struct bmp_header const* header);
struct bmp_header *create_header(const uint32_t width, const uint32_t height);


#endif //BMP_HEADER_HANDLER_H
