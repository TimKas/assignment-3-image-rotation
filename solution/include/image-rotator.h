//
// Created by kasym on 11/14/2023.
//

#ifndef IMAGE_ROTATOR_H
#define IMAGE_ROTATOR_H
#include "../include/image-handler.h"

#include <stdbool.h>

enum rotation_status {
    ROT_OK = 0,
    ROT_ERROR
};

int32_t calc_rotation_number(int32_t const rot_degree);
bool validate_rot_degree(int32_t const rot_degree);
void rotate_by_degree(struct image *to_rotate, struct image **new, int32_t rotation_number);

#endif //IMAGE_ROTATOR_H
