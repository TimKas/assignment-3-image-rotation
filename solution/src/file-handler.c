//
// Created by kasym on 11/14/2023.
//
#include "../include/file-handler.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

struct file_operation_result* open_specified_file_for_reading(char* name) {
    FILE* file = fopen(name, "rb");
    struct file_operation_result* result = malloc(sizeof(struct file_operation_result));
    if (file == NULL) {
        fprintf(stderr, "Error occured while trying to read the source file:" "%s" "\n", name);
        result->status = FILE_OPER_ERROR;
        return result;
    }
    printf("File" "%s" " has been opened successfully\n", name);
    result->status = FILE_OPER_OK;
    result->file = file;
    return result;
}

struct file_operation_result* open_specified_file_for_writing(char* name) {
    FILE* file = fopen(name, "wb");
    struct file_operation_result* result = malloc(sizeof(struct file_operation_result));
    if (file == NULL) {
        fprintf(stderr, "Error occured while trying to write to the target file:" "%s" "\n", name);
        result->status = FILE_OPER_ERROR;
        return result;
    }
    printf("File" "%s" " has been open for writing successfully\n", name);
    result->status = FILE_OPER_OK;
    result->file = file;
    return result;
}

void close_file(FILE* file) {
    if(!file) return;
    fclose(file);
    
}
