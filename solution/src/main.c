
#include "../include/file-handler.h"
#include "../include/bmp-header-handler.h"
#include "../include/image-handler.h"
#include "../include/image-rotator.h"

#include <stdio.h>
#include <stdlib.h>

#define ERROR_EXIT_CODE 1
#define CALM_EXIT_CODE 0
#define SOURCE_FILE_NAME_INDEX 1
#define TARGET_FILE_NAME_INDEX 2
#define NUMBER_OF_ARGS 4

int main( int argc, char** argv ) {
    printf("here we are");
    if(argc != NUMBER_OF_ARGS) {
        fprintf(stderr, "you have provided the wrong number of arguments: there should be [source file] [resulting file] [angle]");
        return ERROR_EXIT_CODE;
    }

    struct file_operation_result* opened_file_res = open_specified_file_for_reading(argv[SOURCE_FILE_NAME_INDEX]);
    if(opened_file_res->status == FILE_OPER_ERROR) return ERROR_EXIT_CODE;
    fprintf(stderr, "file was opened");

    struct bmp_header_operation_result* bmp_read_res = read_bmp_header(opened_file_res->file);
    if(bmp_read_res->status == BMP_HEADER_READ_ERROR) {
        close_file(opened_file_res->file);
        return ERROR_EXIT_CODE;
    }
    fprintf(stderr, "header was read");

    struct image* image = create_new_image(bmp_read_res->header->biHeight, bmp_read_res->header->biWidth);
    if(!image){
        close_file(opened_file_res->file);
        return ERROR_EXIT_CODE;
    }

    enum image_operation_status const read_image_status = read_image_from_file(opened_file_res->file, image);
    if(read_image_status != IMG_OPER_OK) {
        release_image(image);
        close_file(opened_file_res->file);
        free(opened_file_res);
        return ERROR_EXIT_CODE;
    }
    fprintf(stderr, "image was read");

    free(bmp_read_res->header);
    free(bmp_read_res);

    close_file(opened_file_res->file);
    free(opened_file_res);

    int const rot_degree = atoi(argv[3]);
    if(!validate_rot_degree(rot_degree)) {
        release_image(image);
        fprintf(stderr, "angle must be divisible by 90 and lay between -270 and 270");
        return ERROR_EXIT_CODE;
    }
    int const number_of_rotation = calc_rotation_number(rot_degree);
    struct image *rotated_image;
    if(number_of_rotation % 2 == 0)
        rotated_image = create_new_image(image->height, image->width);
    else
        rotated_image = create_new_image(image->width, image->height);

    if(!rotated_image){
        return ERROR_EXIT_CODE;
    }
    rotate_by_degree(image, &rotated_image, number_of_rotation);
    fprintf(stderr, "image was rotated");

    struct bmp_header* new_img_header = create_header(rotated_image->width, rotated_image->height);
    fprintf(stderr, "header was created");

    struct file_operation_result* write_file_res = open_specified_file_for_writing(argv[TARGET_FILE_NAME_INDEX]);
    release_image(image);
    if(write_file_res->status == FILE_OPER_ERROR){
        return ERROR_EXIT_CODE;
    }
    fprintf(stderr, "file for writing was opened");

    if(write_bmp_header(write_file_res->file, new_img_header) != BMP_HEADER_OK ||
            save_image_to_file(write_file_res->file, rotated_image) != IMG_OPER_OK){
        free(new_img_header);
        release_image(rotated_image);
        close_file(write_file_res->file);
        free(write_file_res);
        return ERROR_EXIT_CODE;
    }

    free(new_img_header);

    release_image(rotated_image);
    close_file(write_file_res->file);
    free(write_file_res);

    return CALM_EXIT_CODE;
}



