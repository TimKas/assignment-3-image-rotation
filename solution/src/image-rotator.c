//
// Created by kasym on 11/14/2023.
//

#include "../include/image-rotator.h"
#include "../include/image-handler.h"
#include <stdbool.h>
#include <stdlib.h>

int32_t calc_rotation_number(int32_t const rot_degree) {
    return ((360+rot_degree)%360)/90;
}

bool validate_rot_degree(int32_t const rot_degree) {
    return rot_degree % 90 == 0 && rot_degree <= 270 && rot_degree >= -270;
}

void static rotate_clock_wise_half_pi_internal( struct image const*to_rotate, struct image *new){
    new->height = to_rotate->width;
    new->width = to_rotate->height;
    for (uint32_t h = 0; h < to_rotate->height; h++){
        for (uint32_t w = 0; w < to_rotate->width; w++){
            new->data[(to_rotate->width - 1 - w)*to_rotate->height + h] = to_rotate->data[h*(to_rotate->width)+w];
        }
    }
}

void rotate_by_degree(struct image *to_rotate, struct image **new, int32_t rotation_number){
    if (rotation_number != 0){
        //struct image *new_temp = malloc(sizeof(struct image));
        //new_temp->data = calloc(to_rotate->width*to_rotate->height,sizeof (struct pixel));
        struct image *new_temp = create_new_image(to_rotate->height, to_rotate->width);
        if(new_temp == NULL) {
            fprintf(stderr, "could not create image");
            return;
        }
        rotate_clock_wise_half_pi_internal(to_rotate, new_temp);
        for (int rot = 1; rot < rotation_number; rot++){
            rotate_clock_wise_half_pi_internal(new_temp, (*new));
            struct image *temp = (*new);
            (*new) = new_temp;
            new_temp = temp;
        }
        free((*new)->data);
        (*new)->data = NULL;
        free(*new);
        (*new) = new_temp;
        return;
    }
    (*new)->height = to_rotate->height;
    (*new)->width = to_rotate->width;
    for (uint32_t i = 0; i < to_rotate->height; i++){
        for (uint32_t j = 0; j < to_rotate -> width; j++){
            (*new)->data[i*(to_rotate->width)+j] = to_rotate->data[i*(to_rotate->width)+j];
        }
    }
}



