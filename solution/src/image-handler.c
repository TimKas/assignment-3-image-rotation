//
// Created by kasym on 11/14/2023.
//
#include "../include/image-handler.h"
#include <inttypes.h>

#include <stdlib.h>

uint32_t calc_padding(uint32_t const width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum image_operation_status read_image_from_file(FILE* file, struct image const* to) {
    uint32_t const padding = calc_padding(to->width);
    for (int h = 0; h < to->height; h++) {
        if (fread(to->data + h * to->width, sizeof(struct pixel), to->width, file) == 0) {
            return READ_ERROR;
        }
        if (fseek(file, padding, SEEK_CUR)) {
            return PADDING_ISSUES;
        }
    }
    return IMG_OPER_OK;
}

enum image_operation_status save_image_to_file(FILE* file, struct image const* from) {
    uint32_t const padding = calc_padding(from->width);
    for (int h = 0; h < from->height; h++) {
        if (!fwrite(from->data + h * from->width, sizeof(struct pixel), from->width, file)) {
            return WRITE_ERROR;
        }
        if (padding != 0 && !fwrite(from->data + h * from->width, padding, 1, file)) {
            return PADDING_ISSUES;
        }
    }
    return IMG_OPER_OK;
}

struct image* create_new_image(uint32_t h, uint32_t w) {
    struct image *image = malloc(sizeof(struct image));
    image->width = w;
    image->height = h;
    image->data = calloc(h, (sizeof(struct pixel) * w + calc_padding(w)));
    return image;
}

uint32_t getSizeImage(const uint32_t width, const uint32_t height) {
    return (sizeof(struct pixel) * width + calc_padding(width)) * height;
}

void release_image(struct image* image) {
    if (image == NULL || image->data == NULL) return;
    free(image->data);
    image->data = NULL;
    free(image);
}



