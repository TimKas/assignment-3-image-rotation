//
// Created by kasym on 11/14/2023.
//

#include <stdio.h>
#include <stdlib.h>

#define TYPE 19778
#define  OFF_BITS 54
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24

#include "../include/image-handler.h"
#include "../include/bmp-header-handler.h"

#include <inttypes.h>

struct bmp_header_operation_result *read_bmp_header(FILE *file) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    if (!header) return NULL;
    struct bmp_header_operation_result *new_header = malloc(sizeof(struct bmp_header_operation_result));
    if (!new_header) {
        free(header);
        return NULL;
    }
    if (fread(header, sizeof(struct bmp_header), 1, file) == 0) {
        new_header->status = BMP_HEADER_READ_ERROR;
        return new_header;
    };
    new_header->header = header;
    new_header->status = BMP_HEADER_OK;
    printf("ATTENTION PLEASE: %" PRIu32 " ", new_header->header->biSizeImage);
    return new_header;
}

enum bmp_file_header_status write_bmp_header(FILE *file, struct bmp_header const *header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, file) == 0) {
        return BMP_HEADER_WRITE_ERROR;
    }
    return BMP_HEADER_OK;
}

struct bmp_header *create_header(const uint32_t width, const uint32_t height) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    if (!header) return NULL;

    header->bfType = TYPE;
    header->bfReserved = 0;
    header->bOffBits = OFF_BITS;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biSize = SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biCompression = 0;
    header->biSizeImage = getSizeImage(width, height);
    header->bfileSize = header->biSizeImage + sizeof(struct bmp_header);
    header->biXPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
    header->biYPelsPerMeter = 0;
    return header;
}

